import pandas as pd
import pickle
from joblib import Parallel, delayed
import joblib



model = joblib.load('notamCat.pkl')

vectorizer = joblib.load("vec.pkl")

new_text = ["M0002/23 NOTAMN \n"
"Q) ZLA/QMXHW/IV/M/A/000/999/3251N11423W005 \n"
"A) KLGF \n"
"B) 2301091413 \n"
"C) 2303011300 \n"
"E) TWY A WORK IN PROGRESS CONST WEST EDGE LGTD AND BARRICADED TWY A WORK IN PROGRESS ON SEVERAL HUNDRED FEET AT NORTH END OF TWY A. TWY WIDTH RESTRICTED TO 25'7 IN CONSTRUCTION ZONE DUE TO EXCAVATION ON EAST AND WEST EDGE."]
new_text_vectorized = vectorizer.transform(new_text)
new_label = model.predict(new_text_vectorized)
print(f"Predicted label: {new_label}")